﻿using FYPMap.Models;

namespace FYPMap.Interfaces
{
    public interface IAdminHelper
    {
        bool LogUserIn(string username, string password);
        bool SignUserUp(User userModel);
    }
}

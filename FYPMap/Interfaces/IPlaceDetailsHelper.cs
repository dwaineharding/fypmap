﻿namespace FYPMap.Interfaces
{
    public interface IPlaceDetailsHelper
    {
        PlaceDetailsResponse GetPlaceDetails(string placeId);
    }
}

﻿using FYPMap.Models;

namespace FYPMap.Interfaces
{
    public interface IPlaceSearchHelper
    {
        PlaceSearchResponse GetNearbyPlaces(string lat, string lng, int radius);
    }
}

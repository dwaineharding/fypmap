﻿using FYPMap.ViewModels;

namespace FYPMap.Interfaces
{
    public interface IQuestionHelper
    {
        QuestionModalViewModel GetQuestion(Result correctAnswer);
    }
}

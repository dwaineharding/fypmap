﻿using System.Collections.Generic;

namespace FYPMap.Interfaces
{
    public interface IXmlHelper
    {
        T GetXMLDataFromFile<T>(string path);
    }
}

﻿namespace FYPMap.Interfaces
{
    public interface IHttpRequestHelper
    {
        T GetObjectFromHttpRequest<T>(string requestUrl);
    }
}

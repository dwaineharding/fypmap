﻿using FYPMap.Models;
using System.Collections.Generic;

namespace FYPMap.Interfaces
{
    public interface ILeaderboardHelper
    {
        bool UpdateUserScore(string placeId);
        List<string> GetCurrentUserVisitedPlaces();
        List<UserScores> GetLeaderboard();
    }
}

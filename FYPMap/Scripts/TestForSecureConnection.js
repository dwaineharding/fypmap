﻿$(document).ready(function () {
    if (location.protocol != "https:" && location.hostname != "localhost") {
        $(".btn").attr("disabled", "disabled");
        $("input").attr("disabled", "disabled");
        $(".unsecureMessage").show();
    }
})

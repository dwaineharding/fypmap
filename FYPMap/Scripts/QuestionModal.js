﻿var QuestionModal = (function () {
    $("form").submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/FYPMap/places/SubmitModalQuestion",
            data: {
                selectedAnswer: $("input[name=SelectedAnswer]:checked").val()
            },
            success: function (data) {
                if (data) {
                    $(".modal-body").html("<p>Well Done! You got that one correct!</p>");
                    $(".modal-title").html("Well Done!");
                    $(".modal-footer").hide();
                    MapGame.GetNearbyPlaces();
                    setTimeout(function () { $("#questionModal").modal("hide"); }, 2000);
                } else {
                    $(".wrongAnswerMessage").html("Unlucky! Try again.");
                }
            }
        });
    })
})();
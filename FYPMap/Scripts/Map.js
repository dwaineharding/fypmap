﻿var MapGame = (function() {
    var map, infoWindow, currentLocation, currentLocationMarker;
    var markersArray = [];

    var init = function() {
        infoWindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('defaultMap'),
            {
                center: { lat: -34.397, lng: 150.644 }, //a default location that will change once geolocator active.
                zoom: 18,
                disableDefaultUI: true,
                styles: [
                    {
                        "featureType": "administrative",
                        "elementType": "geometry",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "transit.station.bus",
                        "stylers": [{ "visibility": "off" }]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels",
                        "stylers": [{ visibility: "off" }]
                    }
                ]
            });
        currentLocationMarker = new google.maps.Marker({
            map: map,
            icon: "/FYPMap/Images/CurrentLocationMarker1.png"
        });
        tryGeoLocator();
    }

    var handleLocationError = function(browserHasGeolocation, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation
            ? 'Error: The Geolocation service failed.'
            : 'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

    var getNearbyPlaces = function () {
        $.ajax({
            url: "/FYPMap/Places/GetNearbyPlaces",
            cache: false,
            data: {
                lat: currentLocation.lat,
                lng: currentLocation.lng
            },
            success: function (response) {
                if (response) {
                    for (var i = 0; i < markersArray.length; i++) {
                        markersArray[i].setMap(null);
                    }
                    for (var i = 0; i < response.results.length; i++) {
                        createMarker(response.results[i], response.results[i].Icon == "VISITED");
                    }
                    getImmediatePlaces();
                }
            }
        });
    }
    
    var tryGeoLocator = function() {
        if (navigator.geolocation) { 
            navigator.geolocation.watchPosition(onPositionChange);
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, map.getCenter());
        }
    }

    var createMarker = function(place, visited) {
        var marker = new google.maps.Marker({
            map: map,
            position: { lat: parseFloat(place.Geometry.Location.Lat), lng: parseFloat(place.Geometry.Location.Lng) },
            icon: "/FYPMap/Images/Marker1.png"
        });
        if (visited) {
            marker.setIcon("/FYPMap/Images/VisitedMarker1.png")
        }
        markersArray.push(marker);
    }

    var onPositionChange = function (position) {
        if (position) {
            currentLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            setMapLocation();
            getNearbyPlaces();
            $("#currentLocation").html(currentLocation.lat + ", " + currentLocation.lng);
            var moveCount = parseInt($("#moveCount").html()) + 1;
            $("#moveCount").html(moveCount);
            $("#accuracy").html(position.coords.accuracy);
        }
    }

    var getImmediatePlaces = function () {
        if ($("#questionModal").is(":visible")) {
            return;
        }

        $.ajax({
            url: "/FYPMap/Places/GetImmediatePlaces",
            cache: false,
            data: {
                lat: currentLocation.lat,
                lng: currentLocation.lng,
                //lat: 51.472824, //nursery latlng
                //lng: -3.270126,
            },
            success: function (data) {
                if (data) {
                    $(".modal-dialog").html(data);
                    $("#questionModal").modal("show");
                }
            }
        });  
    }

    var setMapLocation = function () {
        currentLocationMarker.setPosition(currentLocation);
        map.panTo(currentLocation);
        map.setZoom(18);
    }

    return {
        Initialise: init,
        OnPositionChange: onPositionChange,
        GetNearbyPlaces: getNearbyPlaces
    }
})();
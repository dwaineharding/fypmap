﻿using System.Collections.Generic;

namespace FYPMap.Models
{
    public class UserScores
    {
        public string Username { get; set; }
        public List<string> VisitedPlaceIds { get; set; }
    }
}
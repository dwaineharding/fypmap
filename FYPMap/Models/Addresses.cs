﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace FYPMap.Models
{
    [XmlRoot(ElementName = "Addresses")]
    public class Addresses
    {
        [XmlElement(ElementName = "Address")]
        public List<string> Address { get; set; }
    }

}

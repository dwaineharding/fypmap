﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace FYPMap
{
    [XmlRoot(ElementName = "address_component")]
    public class Address_component
    {
        [XmlElement(ElementName = "long_name")]
        public string Long_name { get; set; }
        [XmlElement(ElementName = "short_name")]
        public string Short_name { get; set; }
        [XmlElement(ElementName = "type")]
        public List<string> Type { get; set; }
    }

    [XmlRoot(ElementName = "location")]
    public class Location
    {
        [XmlElement(ElementName = "lat")]
        public string Lat { get; set; }
        [XmlElement(ElementName = "lng")]
        public string Lng { get; set; }
    }

    [XmlRoot(ElementName = "southwest")]
    public class Southwest
    {
        [XmlElement(ElementName = "lat")]
        public string Lat { get; set; }
        [XmlElement(ElementName = "lng")]
        public string Lng { get; set; }
    }

    [XmlRoot(ElementName = "northeast")]
    public class Northeast
    {
        [XmlElement(ElementName = "lat")]
        public string Lat { get; set; }
        [XmlElement(ElementName = "lng")]
        public string Lng { get; set; }
    }

    [XmlRoot(ElementName = "viewport")]
    public class Viewport
    {
        [XmlElement(ElementName = "southwest")]
        public Southwest Southwest { get; set; }
        [XmlElement(ElementName = "northeast")]
        public Northeast Northeast { get; set; }
    }

    [XmlRoot(ElementName = "geometry")]
    public class Geometry
    {
        [XmlElement(ElementName = "location")]
        public Location Location { get; set; }
        [XmlElement(ElementName = "viewport")]
        public Viewport Viewport { get; set; }
    }

    [XmlRoot(ElementName = "review")]
    public class Review
    {
        [XmlElement(ElementName = "time")]
        public string Time { get; set; }
        [XmlElement(ElementName = "text")]
        public string Text { get; set; }
        [XmlElement(ElementName = "author_name")]
        public string Author_name { get; set; }
        [XmlElement(ElementName = "author_url")]
        public string Author_url { get; set; }
        [XmlElement(ElementName = "rating")]
        public string Rating { get; set; }
        [XmlElement(ElementName = "language")]
        public string Language { get; set; }
        [XmlElement(ElementName = "profile_photo_url")]
        public string Profile_photo_url { get; set; }
        [XmlElement(ElementName = "relative_time_description")]
        public string Relative_time_description { get; set; }
    }

    [XmlRoot(ElementName = "open")]
    public class Open
    {
        [XmlElement(ElementName = "day")]
        public string Day { get; set; }
        [XmlElement(ElementName = "time")]
        public string Time { get; set; }
    }

    [XmlRoot(ElementName = "close")]
    public class Close
    {
        [XmlElement(ElementName = "day")]
        public string Day { get; set; }
        [XmlElement(ElementName = "time")]
        public string Time { get; set; }
    }

    [XmlRoot(ElementName = "period")]
    public class Period
    {
        [XmlElement(ElementName = "open")]
        public Open Open { get; set; }
        [XmlElement(ElementName = "close")]
        public Close Close { get; set; }
    }

    [XmlRoot(ElementName = "opening_hours")]
    public class Opening_hours
    {
        [XmlElement(ElementName = "open_now")]
        public string Open_now { get; set; }
        [XmlElement(ElementName = "period")]
        public List<Period> Period { get; set; }
        [XmlElement(ElementName = "weekday_text")]
        public List<string> Weekday_text { get; set; }
    }

    [XmlRoot(ElementName = "a")]
    public class A
    {
        [XmlAttribute(AttributeName = "href")]
        public string Href { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "html_attribution")]
    public class Html_attribution
    {
        [XmlElement(ElementName = "a")]
        public A A { get; set; }
    }

    [XmlRoot(ElementName = "photo")]
    public class Photo
    {
        [XmlElement(ElementName = "photo_reference")]
        public string Photo_reference { get; set; }
        [XmlElement(ElementName = "width")]
        public string Width { get; set; }
        [XmlElement(ElementName = "height")]
        public string Height { get; set; }
        [XmlElement(ElementName = "html_attribution")]
        public Html_attribution Html_attribution { get; set; }
    }

    [XmlRoot(ElementName = "span")]
    public class Span
    {
        [XmlAttribute(AttributeName = "class")]
        public string Class { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "adr_address")]
    public class Adr_address
    {
        [XmlElement(ElementName = "span")]
        public List<Span> Span { get; set; }
    }

    [XmlRoot(ElementName = "result")]
    public class Result
    {
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "vicinity")]
        public string Vicinity { get; set; }
        [XmlElement(ElementName = "type")]
        public List<string> Type { get; set; }
        [XmlElement(ElementName = "formatted_phone_number")]
        public string Formatted_phone_number { get; set; }
        [XmlElement(ElementName = "formatted_address")]
        public string Formatted_address { get; set; }
        [XmlElement(ElementName = "address_component")]
        public List<Address_component> Address_component { get; set; }
        [XmlElement(ElementName = "geometry")]
        public Geometry Geometry { get; set; }
        [XmlElement(ElementName = "rating")]
        public string Rating { get; set; }
        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
        [XmlElement(ElementName = "icon")]
        public string Icon { get; set; }
        [XmlElement(ElementName = "reference")]
        public string Reference { get; set; }
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "international_phone_number")]
        public string International_phone_number { get; set; }
        [XmlElement(ElementName = "website")]
        public string Website { get; set; }
        [XmlElement(ElementName = "review")]
        public List<Review> Review { get; set; }
        [XmlElement(ElementName = "opening_hours")]
        public Opening_hours Opening_hours { get; set; }
        [XmlElement(ElementName = "utc_offset")]
        public string Utc_offset { get; set; }
        [XmlElement(ElementName = "photo")]
        public List<Photo> Photo { get; set; }
        [XmlElement(ElementName = "adr_address")]
        public Adr_address Adr_address { get; set; }
        [XmlElement(ElementName = "place_id")]
        public string Place_id { get; set; }
        [XmlElement(ElementName = "scope")]
        public string Scope { get; set; }
    }

    [XmlRoot(ElementName = "PlaceDetailsResponse")]
    public class PlaceDetailsResponse
    {
        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "result")]
        public Result Result { get; set; }
    }

}

﻿using FYPMap.ViewModels;

namespace FYPMap.Models
{
    public class User
    {
        public User(SignUpViewModel signUpViewModel)
        {
            Forename = signUpViewModel.Forename;
            Surname = signUpViewModel.Surname;
            EmailAddress = signUpViewModel.EmailAddress;
            Username = signUpViewModel.Username;
            Password = signUpViewModel.Password;
        }
        public User() { }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
﻿using FYPMap.Interfaces;
using FYPMap.Models;
using FYPMap.ViewModels;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace FYPMap.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IAdminHelper _adminHelper;
        private readonly ILeaderboardHelper _leaderboardHelper;
        public HomeController(IAdminHelper adminHelper, ILeaderboardHelper leaderboardHelper)
        {
            _adminHelper = adminHelper;
            _leaderboardHelper = leaderboardHelper;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Signup()
        {
            return View();
        }

        public ActionResult Map()
        {
            var connString = $"https://maps.googleapis.com/maps/api/js?key=";
            connString += $"{WebConfigurationManager.AppSettings["APIKey"]}&callback=MapGame.Initialise";
            return View((object)connString);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
                return View(loginViewModel);

            var userLoggedIn = _adminHelper.LogUserIn(loginViewModel.Username, loginViewModel.Password);

            if (userLoggedIn)
            {
                FormsAuthentication.SetAuthCookie(loginViewModel.Username, false);
                return RedirectToAction("Map");
            }
            else
            {
                ModelState.AddModelError("", "Incorrect username/password combination, try again!");
                loginViewModel.Password = string.Empty;
                return View(loginViewModel);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SignUp(SignUpViewModel signUpViewModel)
        {
            if (!ModelState.IsValid)
                return View(signUpViewModel);

            var userModel = new User(signUpViewModel);
            var userSignedUp = _adminHelper.SignUserUp(userModel);
            if (userSignedUp)
            {
                FormsAuthentication.SetAuthCookie(signUpViewModel.Username, false);
                return RedirectToAction("Map");
            }
            else
            {
                ModelState.AddModelError("", "Username already exists.");
                return View(signUpViewModel);
            }
        }

        public ActionResult Leaderboard()
        {
            var leaderboard = _leaderboardHelper.GetLeaderboard().OrderByDescending(x=>x.VisitedPlaceIds.Count);
            var currentUser = leaderboard.FirstOrDefault(x => x.Username.ToLower() == HttpContext.User.Identity.Name.ToLower());
            if (currentUser != null) currentUser.Username += " (me)";
            return View(leaderboard.ToList());
        }

        [AllowAnonymous]
        public ActionResult GetTermsAndConditions()
        {
            string text = System.IO.File.ReadAllText(Server.MapPath("~/TermsAndConditions.txt"));
            return Json(new { content = text }, JsonRequestBehavior.AllowGet);
        }
    }
}
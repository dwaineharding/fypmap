﻿using System;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using FYPMap.Interfaces;
using FYPMap.ViewModels;

namespace FYPMap.Controllers
{
    [Authorize]
    public class PlacesController : Controller
    {
        private readonly IPlaceDetailsHelper _placeDetailsHelper;
        private readonly IPlaceSearchHelper _placeSearchHelper;
        private readonly IQuestionHelper _questionHelper;
        private readonly ILeaderboardHelper _leaderboardHelper;

        public PlacesController(IPlaceDetailsHelper placeDetailsHelper, 
            IPlaceSearchHelper placeSearchHelper, IQuestionHelper questionHelper, ILeaderboardHelper leaderboardHelper)
        {
            _placeDetailsHelper = placeDetailsHelper;
            _placeSearchHelper = placeSearchHelper;
            _questionHelper = questionHelper;
            _leaderboardHelper = leaderboardHelper;
        }

        public ActionResult GetNearbyPlaces(string lat, string lng)
        {
            var nearbyRadius = Convert.ToInt32(WebConfigurationManager.AppSettings["NearbyRadius"]);
            var placesSearch = _placeSearchHelper.GetNearbyPlaces(lat, lng, nearbyRadius);
            var currentUserVisitedPlaces = _leaderboardHelper.GetCurrentUserVisitedPlaces();
            if (placesSearch.Status == "OK")
            {
                foreach (var place in currentUserVisitedPlaces)
                {
                    var placeInSearch = placesSearch.Result.FirstOrDefault(x => x.Place_id == place);
                    if (placeInSearch != null)
                        placeInSearch.Icon = "VISITED";
                }
                HttpContext.Session["NearbyPlaceNames"] = placesSearch.Result.Select(x => x.Name).ToList();
                return Json(new { results = placesSearch.Result}, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public ActionResult GetImmediatePlaces(string lat, string lng)
        {
            var immediateRadius = Convert.ToInt32(WebConfigurationManager.AppSettings["ImmediateRadius"]);
            var placesSearch = _placeSearchHelper.GetNearbyPlaces(lat, lng, immediateRadius);
            var currentUserVisitedPlaces = _leaderboardHelper.GetCurrentUserVisitedPlaces();
            foreach (var place in currentUserVisitedPlaces)
            {
                var placeInSearch = placesSearch.Result.FirstOrDefault(x => x.Place_id == place);
                if (placeInSearch != null)
                    placesSearch.Result.Remove(placeInSearch);
            }

            if (placesSearch.Status == "OK" && placesSearch.Result.Count > 0)
            {
                var result = placesSearch.Result.First();
                var placeDetails = _placeDetailsHelper.GetPlaceDetails(result.Place_id).Result;
                var questionModalViewModel = _questionHelper.GetQuestion(placeDetails);
                return PartialView("_QuestionModal", questionModalViewModel);
            }

            return null;
        }

        public ActionResult SubmitModalQuestion(string selectedAnswer)
        {
            var questionModalViewModel = (QuestionModalViewModel)HttpContext.Session["QuestionModalViewModel"];
            if (selectedAnswer == questionModalViewModel.CorrectAnswer)
            {
                _leaderboardHelper.UpdateUserScore(questionModalViewModel.PlaceId);
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
    }
}
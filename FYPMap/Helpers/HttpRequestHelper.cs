﻿using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using FYPMap.Interfaces;

namespace FYPMap.Helpers
{
    public class HttpRequestHelper : IHttpRequestHelper
    {
        public T GetObjectFromHttpRequest<T>(string requestUrl)
        {
            var request = WebRequest.Create(requestUrl) as HttpWebRequest;
            var response = (HttpWebResponse)request?.GetResponse();
            var encoding = Encoding.ASCII;
            string responseText;
            using (var reader = new StreamReader(response.GetResponseStream(), encoding))
            {
                responseText = reader.ReadToEnd();
            }

            var serializer = new XmlSerializer(typeof(T));
            var rdr = new StringReader(responseText);
            return (T)serializer.Deserialize(rdr);
        }
    }
}
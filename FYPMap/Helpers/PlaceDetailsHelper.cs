﻿
using FYPMap.Interfaces;
using System.Web.Configuration;

namespace FYPMap.Helpers
{
    public class PlaceDetailsHelper : IPlaceDetailsHelper
    {
        //private readonly string _apiKey = "AIzaSyDqwQYcZRPDDVCO6gaVCjm9UiqhV34RkBQ";
        private readonly string _apiKey = WebConfigurationManager.AppSettings["APIKey"];
        private readonly IHttpRequestHelper _httpRequestHelper;

        public PlaceDetailsHelper(IHttpRequestHelper httpRequestHelper)
        {
            _httpRequestHelper = httpRequestHelper;
        }

        public PlaceDetailsResponse GetPlaceDetails(string placeId)
        {
            var baseUrl = "https://maps.googleapis.com/maps/api/place/details/xml?";
            baseUrl += $"placeid={placeId}&key={_apiKey}";

            return _httpRequestHelper.GetObjectFromHttpRequest<PlaceDetailsResponse>(baseUrl);
        }


    }
}
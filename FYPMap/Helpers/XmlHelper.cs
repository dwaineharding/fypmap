﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using FYPMap.Interfaces;
using FYPMap.Models;

namespace FYPMap.Helpers
{
    public class XmlHelper : IXmlHelper, IAdminHelper, ILeaderboardHelper
    {
        public List<string> GetCurrentUserVisitedPlaces()
        {
            var leaderboard = GetLeaderboard();
            return leaderboard.FirstOrDefault(x => x.Username.ToLower() == HttpContext.Current.User.Identity.Name.ToLower())?.VisitedPlaceIds ?? new List<string>();
        }

        public T GetXMLDataFromFile<T>(string path)
        {
            var xmlString = File.ReadAllText(path);
            T result;
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(xmlString))
            {
                result = (T)serializer.Deserialize(reader);
            }
            return result;
        }

        public bool LogUserIn(string username, string password)
        {
            var users = GetXMLDataFromFile<List<User>>(HttpContext.Current.Server.MapPath("~/XMLData/Users.xml"));
            var user = users.SingleOrDefault(x => x.Username.ToLower() == username.ToLower() && x.Password == password);
            return user != null ? true : false;
        }

        public bool SignUserUp(User userModel)
        {
            var path = HttpContext.Current.Server.MapPath("~/XMLData/Users.xml");
            var existingUsers = GetXMLDataFromFile<List<User>>(path);
            if (existingUsers.SingleOrDefault(x => x.Username.ToLower() == userModel.Username.ToLower()) != null) return false;

            existingUsers.Add(userModel);

            return WriteToXmlFile(existingUsers, path);
        }

        private bool WriteToXmlFile<T>(T objectToWrite, string path)
        {
            var writer = new XmlSerializer(typeof(T));

            string xml;
            using (var textWriter = new StringWriter())
            {
                writer.Serialize(textWriter, objectToWrite);
                xml = textWriter.ToString();
            }
            File.WriteAllText(path, xml);
            return true;
        }

        public bool UpdateUserScore(string placeId)
        {
            var path = HttpContext.Current.Server.MapPath("~/XMLData/Leaderboard.xml");
            var existingLeaderboard = GetXMLDataFromFile<List<UserScores>>(path);
            var loggedInUsername = HttpContext.Current.User.Identity.Name;
            var user = existingLeaderboard.FirstOrDefault(x => x.Username.ToLower() == loggedInUsername.ToLower());

            if (user == null) user = new UserScores { Username = loggedInUsername, VisitedPlaceIds = new List<string>() };
            else existingLeaderboard.Remove(user);

            user.VisitedPlaceIds.Add(placeId);
            existingLeaderboard.Add(user);
            return WriteToXmlFile(existingLeaderboard, path);
        }

        public List<UserScores> GetLeaderboard()
        {
            return GetXMLDataFromFile<List<UserScores>>(HttpContext.Current.Server.MapPath("~/XMLData/Leaderboard.xml"));
        }
    }
}
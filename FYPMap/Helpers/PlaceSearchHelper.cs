﻿using FYPMap.Interfaces;
using FYPMap.Models;
using System.Linq;
using System.Web.Configuration;

namespace FYPMap.Helpers
{
    public class PlaceSearchHelper : IPlaceSearchHelper
    {
        //private readonly string _apiKey = "AIzaSyDqwQYcZRPDDVCO6gaVCjm9UiqhV34RkBQ";
        private readonly string _apiKey = WebConfigurationManager.AppSettings["APIKey"];
        private readonly IHttpRequestHelper _httpRequestHelper;

        public PlaceSearchHelper(IHttpRequestHelper httpRequestHelper)
        {
            _httpRequestHelper = httpRequestHelper;
        }

        public PlaceSearchResponse GetNearbyPlaces(string lat, string lng, int radius)
        {
            var baseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?";
            baseUrl += $"location={lat},{lng}&radius={radius.ToString()}&key={_apiKey}";

            var placeSearchResponse = _httpRequestHelper.GetObjectFromHttpRequest<PlaceSearchResponse>(baseUrl);
            placeSearchResponse.Result = placeSearchResponse.Result.GroupBy(x => x.Name).Select(y=>y.First()).ToList();
            placeSearchResponse.Result = placeSearchResponse.Result.FindAll(x => x.Type.Contains("point_of_interest") || x.Type.Contains("establishment"));
            placeSearchResponse.Result.RemoveAll(x => x.Type.Contains("bus_station") || x.Type.Contains("transit_station"));

            return placeSearchResponse;
        }
    }
}
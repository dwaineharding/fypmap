﻿using FYPMap.Interfaces;
using FYPMap.Models;
using FYPMap.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;

namespace FYPMap.Helpers
{
    public class QuestionHelper : IQuestionHelper
    {
        private readonly IXmlHelper _xmlHelper;
        private readonly Random _random;
        public QuestionHelper(IXmlHelper xmlHelper)
        {
            _xmlHelper = xmlHelper;
            _random = new Random();
        }
        public QuestionModalViewModel GetQuestion(Result placeDetails)
        {
            string correctAnswer;
            //if(_random.Next() % 2 == 0)
            //{
            //    correctAnswer = placeDetails.Formatted_address;
            //    return GetAddressQuestion(correctAnswer, placeDetails.Place_id);
            //}

            correctAnswer = placeDetails.Name;
            return GetPlaceNameQuestion(correctAnswer, placeDetails.Place_id);
        }
        private QuestionModalViewModel GetAddressQuestion(string correctAnswer, string placeId)
        {
            var question = "What is the address of the place you are within 10 metres of?";
            var addresses = _xmlHelper.GetXMLDataFromFile<Addresses>(HttpContext.Current.Server.MapPath("~/XMLData/Addresses.xml")).Address;            
            var answers = new List<string>();
            GenerateAnswers(answers, addresses, correctAnswer);
            return new QuestionModalViewModel(question, answers, correctAnswer, placeId);
        }
        private QuestionModalViewModel GetPlaceNameQuestion(string correctAnswer, string placeId)
        {
            var question = "What is the name of the place you are within 10 metres of?";
            var placeNames = (List<string>)HttpContext.Current.Session["NearbyPlaceNames"];
            var answers = new List<string>();
            GenerateAnswers(answers, placeNames, correctAnswer);
            return new QuestionModalViewModel(question, answers, correctAnswer, placeId);
        }
        private void GenerateAnswers(List<string> answers, List<string> potentialAnswers, string correctAnswer)
        {
            potentialAnswers.Remove(correctAnswer);
            for (int i = 0; i < 4; i++)
            {
                if (potentialAnswers.Count < 1) return;
                var randomNumber = _random.Next(potentialAnswers.Count - 1);
                answers.Add(potentialAnswers[randomNumber]);
                potentialAnswers.Remove(potentialAnswers[randomNumber]);
                if (randomNumber % 3 == 0 && !answers.Contains(correctAnswer)) answers.Add(correctAnswer);
            }

            if (!answers.Contains(correctAnswer)) answers.Add(correctAnswer);
        }
    }
}
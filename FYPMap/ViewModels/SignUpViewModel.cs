﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FYPMap.ViewModels
{
    public class SignUpViewModel
    {
        [Required]
        public string Forename { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [Compare("Password", ErrorMessage = "Passwords do not match, try again!")]
        public string ConfirmPassword { get; set; }

        [Required]
        //[DisplayName("Tick to agree to the T&C's")] Moved to inline html to have pop up link.
        [Range(typeof(bool), "true", "true", ErrorMessage = "You must agree to the terms and conditions.")]
        public bool AgreeToTerms { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Web;

namespace FYPMap.ViewModels
{
    public class QuestionModalViewModel
    {
        public QuestionModalViewModel(string question, List<string> answers, string correctAnswer, string placeId)
        {
            Question = question;
            Answers = answers;
            CorrectAnswer = correctAnswer;
            PlaceId = placeId;

            HttpContext.Current.Session["QuestionModalViewModel"] = this;
        }

        public QuestionModalViewModel() { }

        public string Question { get; set; }
        public List<string> Answers { get; set; }
        public string CorrectAnswer { get; set; }
        public string SelectedAnswer { get; set; }
        public string PlaceId { get; set; }
    }
}